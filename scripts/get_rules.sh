#!/bin/bash

for FILE in *; do
  jq -r '.abilities' $FILE | sed -E '/^\{|\}/d' | sed -E 's/$/,/' | sed -E 's/,,/,/' >> /home/tim/workspace/kriegzeug_data/rules_text.json; 
done
