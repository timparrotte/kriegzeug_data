#!/bin/bash

awk -F'\":' '{ 
    gsub(/^  /,"")
    gsub(/\t/," ")
    gsub(/  /," ")
    gsub(/[\[\]\(\)\*]/,"")
    gsub(/  Action/, "")
    print "{\n\t\"name\": "$1"\","
    print "\t\"edition\": 3,"
    print "\t\"text\":"$2"\","
    print "}"

}' rules_text.json
