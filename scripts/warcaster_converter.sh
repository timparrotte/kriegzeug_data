#!/bin/bash

# This is the header that already exists for all(?) models in the IronCodex.
#echo "--- !warmahordes_opendata.Model"
#echo "ppid: "
#echo "name: $(jq '.name' $1)"
#echo "role: warcaster"
#echo "factions:"
#echo "- cygnar"
#echo "scans: 4"
#echo ""

# Here begins unique data.


echo "revision: $(jq ".revision")"
echo ""

# Keywords

echo "keywords:"
jq ".keywords | .[]" $1 | sed 's/^/- /' | sed 's/"//g' | \
    tr '[:upper:]' '[:lower:]'
echo ""

# Basic Stats

echo "battlegroup_points: $(jq '."warjack points"' $1)"
echo "field_allowance: $(jq '.FA' $1)" | sed 's/"//g'
echo ""
echo "models:"
echo "- name: $(jq '."character short name"' $1)" | sed 's/"//g'
echo "  stats:"
echo "    spd: $(jq '.SPD' $1)"
echo "    str: $(jq '.STR' $1)"
echo "    mat: $(jq '.MAT' $1)"
echo "    rat: $(jq '.RAT' $1)"
echo "    def: $(jq '.DEF' $1)"
echo "    arm: $(jq '.ARM' $1)"
echo "    cmd: $(jq '.CMD' $1)"
echo "    focus: $(jq '.focus' $1)"

# Advantages / Icons

echo "  advantages:"
jq ".icons | .[] | ascii_downcase" $1 | sed 's/ /_/g' |\
    sed 's/^/  - /' | sed 's/"//g'

# More Basic Stats
echo "  base_size: $(jq '."base size"' $1)"

# Weapons

echo "  weapons:"
sed 's/P+S/ps/g' $1 | yq -P '.attacks | to_entries | .[]' |
    sed 's/key: /  - name: /g' | \
    sed '/^value/d' | \
    sed 's/^  Type/    type/' | \
    sed 's/^  Number/    number/' | \
    sed 's/^  RNG/    rng/' | \
    sed 's/^  ROF/    rof/' | \
    sed 's/^  POW/    pow/' | \
    sed 's/icons/  qualities/' | \
    sed "s/'//g" | \
    sed '/null/d' | \
    sed '/: -/d' | \
    tr '[:upper:]' '[:lower:]'

# Abilities

echo "  rules:"
jq '.abilities | to_entries | .[] | {"-":.key}' $1 | \
    sed -E '/\{|\}/d' | \
    sed 's/"//g' | \
    sed 's/^  -:/  -/' | \
    tr '[:upper:]' '[:lower:]'

# Spells

echo "  spells:"
jq '.spells | to_entries | .[] | {"-":.key}' $1 | \
    sed -E '/\{|\}/d' | \
    sed 's/"//g' | \
    sed 's/^  -:/  -/' | \
    tr '[:upper:]' '[:lower:]'

# Feat

echo "  feat:"
jq '.feat | to_entries | .[] | {"    name":.key}' $1 | \
    sed -E '/\{|\}/d' | \
    sed 's/"//g'
echo "      flavor: >"
echo "      description: >"
jq '.feat | to_entries | .[] | {"      ":.value}' $1 | \
    sed 's/://' | \
    sed -E '/\{|\}/d' | \
    sed 's/"//g' | \
    sed 's/Immunity:/Immunity\:/g' |\
    sed 's/Damage Type:/Damage Type\"/g'

