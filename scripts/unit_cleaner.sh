#!/bin/bash

#cd /home/tim/Workspace/kriegzeug_data/mkiii_data/cards/
files=$(grep -Rl  "Unit")
for f in $files; do
  jq '."unit size" |= (add | . as $in | reduce keys_unsorted[] as $k ([]; . + [{size: $k} + ($in[$k]|add) ] ))' $f > /home/tim/Workspace/kriegzeug_data/mkiii_data/fixed_units/$f
done
