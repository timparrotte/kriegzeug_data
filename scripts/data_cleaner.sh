#!/bin/bash

for f in ./*json; do
    jq '.models |= map(to_entries[] | {"model name": .key} + .value)
  | .models[].attacks |= map(to_entries[] | {"attack name": .key} + .value)' $f > /home/tim/Workspace/kriegzeug_data/mkiii_data/all_cards/$f
done
