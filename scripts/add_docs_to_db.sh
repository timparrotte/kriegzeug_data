#!/bin/bash

## This script takes all json files in the working directory and throws them into a MongoDB collection within a database.
cd /home/tim/Workspace/kriegzeug_data/mkiii_data/cards/
for f in ./*json; do
    printf "Adding $f to database.\n"
    mongoimport --uri mongodb://localhost:27017/kriegzeug --collection cards --type json --file $f #>> /home/tim/Workspace/kriegzeug_data/find_erroring_files.txt
done
